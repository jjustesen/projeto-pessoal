import React from 'react'
import { Header } from './components/molecules/Header'
import { DescriptionBox } from './components/molecules/DesciptionBox'
import GlobalStyle from './styles/globalStyles'
import { SptBox } from './components/elements/Box'
import { Parallax } from 'react-scroll-parallax'

function App() {
  return (
    <>
      <GlobalStyle />
      <Header />
      <SptBox width={350} height={400} bg="grey" position="fixed" bottom={0} right={0}>
        <DescriptionBox />
      </SptBox>
      <Parallax y={[-120, 120]}>
        <SptBox width={400}>
          <DescriptionBox />
        </SptBox>
      </Parallax>
      <Parallax y={[-50, 220]} x={["150px", "50px"]}>
        <SptBox width={500}>
          <DescriptionBox />
        </SptBox>
      </Parallax>
      <SptBox width={350} height={1400} bg="grey" bottom={0} right={0}></SptBox>
    </>
  )
}

export default App
