import systemPropTypes from '@styled-system/prop-types'

import styled from 'styled-components'
import { flexbox, border } from 'styled-system'

import { SptBox } from '../Box'

export const SptFlex = styled(SptBox)`
  ${flexbox};
  ${border};
`

SptFlex.displayName = 'SptFlex'

SptFlex.propTypes = {
  ...SptBox.propTypes,
  ...systemPropTypes.flexbox
}

SptFlex.defaultProps = {
  display: 'flex'
}
