import styled, { css } from 'styled-components'

import { theme, ifProp } from '@sponte/lib-utils/dist/theme/tools'

import { SptBox } from '../Box'
import { SptFlex } from '../Flex'

export const SptContainer = styled(SptBox)`
  ${theme('mediaQueries.up.mobile')} {
    max-width: ${theme('breakpoints.mobile')};
  }

  ${theme('mediaQueries.up.tablet')} {
    max-width: ${theme('breakpoints.tablet')};
  }

  ${theme('mediaQueries.up.web')} {
    max-width: ${theme('breakpoints.web')};
  }

  ${theme('mediaQueries.up.hd')} {
    max-width: ${theme('breakpoints.hd')};
  }

  ${theme('mediaQueries.up.fullhd')} {
    max-width: ${theme('breakpoints.fullhd')};
  }

  ${ifProp(
    'fluid',
    css`
      max-width: none;
    `
  )};
`

SptContainer.displayName = 'SptContainer'

SptContainer.propTypes = {
  ...SptBox.propTypes
}

SptContainer.defaultProps = {
  px: 5,
  mx: 'auto',
  width: 1
}

export const SptRow = styled(SptFlex)``

SptRow.displayName = 'SptRow'

SptRow.propTypes = {
  ...SptFlex.propTypes
}

SptRow.defaultProps = {
  mx: -5,
  flexWrap: 'wrap'
}

export const SptCol = styled(SptBox)``

SptCol.displayName = 'SptCol'

SptCol.propTypes = {
  ...SptBox.propTypes
}

SptCol.defaultProps = {
  px: 5,
  my: 5,
  width: 1
}
