import React from 'react'

import { render, cleanup } from '@sponte/lib-utils/dist/test-utils'

import { SptRow, SptCol } from '.'

afterEach(cleanup)

describe('SptRow and SptCol', () => {
  it('deve renderizar e ser compativel com a versao anterior', () => {
    const { asFragment } = render(
      <SptRow>
        <SptCol>Sponte</SptCol>
      </SptRow>
    )
    expect(asFragment()).toMatchSnapshot()
  })
})
