import systemPropTypes from '@styled-system/prop-types'
import PropTypes from 'prop-types'

import styled from 'styled-components'
import { space, color, fontFamily, fontWeight, textAlign, letterSpacing, fontSize, layout } from 'styled-system'

export const SptText = styled.span`
  ${space};
  ${color};
  ${fontFamily};
  ${fontWeight};
  ${textAlign};
  ${letterSpacing};
  ${fontSize};
  ${layout};
`

SptText.displayName = 'SptText'

SptText.propTypes = {
  truncate: PropTypes.bool,
  ...systemPropTypes.space,
  ...systemPropTypes.color,
  ...systemPropTypes.typography
}

SptText.defaultProps = {
  variant: 'default',
  fontFamily: 'default'
}
