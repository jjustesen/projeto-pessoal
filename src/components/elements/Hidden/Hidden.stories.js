import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { text } from '@storybook/addon-knobs'

import styles from '../../../../../.storybook/addons/styles'

import { SptHiddenStyled, SptHidden } from '../../'

storiesOf('Components|Elements/Hidden', module)
  .addDecorator(styles('center'))
  .addParameters({
    info: {
      propTables: [SptHidden]
    }
  })

  .add('Basic usage', () => {
    return (
      <SptHidden>
        {text('content', 'My Component')}
      </SptHidden>
    )
  })
