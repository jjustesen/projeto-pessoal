import React from 'react'

import { render, cleanup } from '@sponte/lib-utils/dist/test-utils'

import { SptHidden } from '../..'

afterEach(cleanup)

describe('SptHidden', () => {
  it('deve renderizar e ser compativel com a versao anterior', () => {
    const { asFragment } = render(<SptHidden>Sponte</SptHidden>)
    expect(asFragment()).toMatchSnapshot()
  })
})
