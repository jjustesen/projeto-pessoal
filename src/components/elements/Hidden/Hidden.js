import styled from 'styled-components'

import { SptBox } from '../Box'

export const SptHidden = styled(SptBox)`
  border: 0px;
  clip: rect(0px, 0px, 0px, 0px);
  height: 1px;
  width: 1px;
  margin: -1px;
  padding: 0px;
  overflow: hidden;
  white-space: nowrap;
  position: absolute;
`

SptHidden.displayName = 'SptHidden'

SptHidden.propTypes = {}

SptHidden.defaultProps = {}
