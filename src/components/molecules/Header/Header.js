import React from 'react'
import { SptBox } from '../../elements/Box'
import { SptFlex } from '../../elements/Flex'
import { SptText } from '../../elements/Text'
import NavBar from './NavBar'

export const Header = (props) => {
  return (
    <>
      <SptBox>
        <SptFlex justifyContent="space-between">
          <SptBox width={130} m={32}>
            <SptText fontSize={20} fontWeight={400}>
              PORTIFOLIO OF NAME HERE
            </SptText>
          </SptBox>
          <NavBar />
        </SptFlex>
      </SptBox>
    </>
  )
}

export default Header
