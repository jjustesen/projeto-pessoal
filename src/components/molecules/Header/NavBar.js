import React from 'react'
import { SptBox } from '../../elements/Box'
import { SptFlex } from '../../elements/Flex'
import { SptText } from '../../elements/Text'

export const NavBar = (props) => {
  return (
    <SptFlex alignItems="center">
      <SptBox m={16}>
        <SptText fontSize={20} fontWeight={400}>
          HOME
        </SptText>
      </SptBox>
      <SptBox m={16}>
        <SptText fontSize={20} fontWeight={400}>
          GALLERY
        </SptText>
      </SptBox>
      <SptBox m={16}>
        <SptText fontSize={20} fontWeight={400}>
          ABOUT US
        </SptText>
      </SptBox>
      <SptBox m={16}>
        <SptText fontSize={20} fontWeight={400}>
          CONTACT
        </SptText>
      </SptBox>
    </SptFlex>
  )
}

export default NavBar
