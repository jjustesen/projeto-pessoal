import React from 'react'
import { SptBox } from '../../elements/Box'
import { SptFlex } from '../../elements/Flex'
import { SptText } from '../../elements/Text'

export const DescriptionBox = (props) => {
  return (
    <>
      <SptBox p={26} pt={0}>
        <SptFlex flexDirection="column">
          <SptText fontSize={30} fontWeight={700} mt={16}>
            Lorem Ipsum
          </SptText>
          <SptText fontSize={20} fontWeight={400} mt={12}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam
          </SptText>
        </SptFlex>
      </SptBox>
    </>
  )
}

export default DescriptionBox
