import React from 'react'
import styled from 'styled-components'
import { SptBox } from '../../elements/Box'
import { SptFlex } from '../../elements/Flex'
import { SptText } from '../../elements/Text'

export const SptPhoto = styled(SptBox)`
  .Bigger {
    width: 100%;
  }
`

// const PhotoFormat = (props) => {
//   return (
//     <>
//       <SptPhoto>
//     </>
//   )
// }

export default SptPhoto
